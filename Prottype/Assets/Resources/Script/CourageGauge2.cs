﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CourageGauge2 : MonoBehaviour {

    private GameObject _player2;
    private GameObject _courageGauge2;
    private Player2 _player2Script;

    // Use this for initialization
    void Start () {
        _player2 = GameObject.Find ("Player2");
        _courageGauge2 = GameObject.Find ("CourageGauge2");
        _player2Script = GameObject.Find ("Player2").GetComponent<Player2> ();
    }

    // Update is called once per frame
    void Update () {
        UpdateDisplay ();
    }

    void UpdateDisplay () {
        _courageGauge2.GetComponent<Text> ().text = "Courage:" + ((int)(_player2Script.m_hp)).ToString ();
    }
}

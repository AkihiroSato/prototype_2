﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CourageGauge1 : MonoBehaviour {

    private GameObject _player1;
    private GameObject _courageGauge1;
    private Player1 _player1Script;

    // Use this for initialization
    void Start () {
        _player1 = GameObject.Find ("Player1");
        _courageGauge1 = GameObject.Find ("CourageGauge1");
        _player1Script = GameObject.Find ("Player1").GetComponent<Player1> ();
    }

    // Update is called once per frame
    void Update () {
        UpdateDisplay ();
    }

    void UpdateDisplay () {
        _courageGauge1.GetComponent<Text> ().text = "Courage:" + ((int)(_player1Script.m_hp)).ToString ();
    }
}

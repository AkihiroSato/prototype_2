﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {
    //======================================================================================================
    // 定数
    //======================================================================================================


    //======================================================================================================
    // 変数
    //======================================================================================================
    //--public-----------------------------------
    public GameObject[] m_players;


    //--private----------------------------------
    private int[] m_fingerID;
    private bool[] m_saveFingerID;
    private Vector2[] m_touchPos;
    private Vector2 m_p2actButtonStartPos;
    private GameObject m_touchImage;
    private Vector2[] m_playerMoveField;


    //=======================================================================================================
    // プロパティマネージャ
    //=======================================================================================================
    delegate void SetTouchImageHandle(Vector2 pos);
    SetTouchImageHandle setTouchImageHandle;

    //=======================================================================================================
    // 関数
    //=======================================================================================================
    //--------------------------------------------------------------------------
    // 初期化処理
    void Start()
    {
        m_fingerID = new int[2];
        m_saveFingerID = new bool[10];
        m_touchPos = new Vector2[2];
        m_playerMoveField = new Vector2[2];

        for(int i = 0; i < m_fingerID.Length; i++)
        {
            m_fingerID[i] = -1;
        }
        for(int i = 0; i < m_saveFingerID.Length; i++)
        {
            m_saveFingerID[i] = false;
        }

        m_p2actButtonStartPos = new Vector2(Screen.width - 200.0f, Screen.height - 200.0f);

        m_touchImage = Resources.Load("Prefab/TouchImage") as GameObject;

        // プレイヤーのタッチ範囲を設定
        m_playerMoveField[0] = new Vector2(0.0f, Screen.height * 0.5f);
        m_playerMoveField[1] = new Vector2(Screen.width * 0.5f, 0.0f);

    }


    //--------------------------------------------------------------------------
    // 更新処理
    void Update()
    {
        for (int i = 0; i < 10; i++)
        {
            m_saveFingerID[i] = false;
        }

        int touchCount = Input.touchCount;
        for(int i = 0; i < touchCount; i++)
        {
            Touch touch = Input.GetTouch(i);
            m_saveFingerID[touch.fingerId] = true;

            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // 移動検知
            // 1P側
            if (m_fingerID[0] < 0 && Utility.CheckRange(touch.position, m_playerMoveField[0], Screen.width * 0.5f, Screen.height * 0.5f))
            {
                GameObject obj = GameObject.Instantiate(m_touchImage) as GameObject;
                setTouchImageHandle = obj.GetComponent<TouchImage>().PlaceImage;
                setTouchImageHandle(touch.position);
                m_fingerID[0] = touch.fingerId;
                m_touchPos[0] = touch.position;
                
            }

            if(m_fingerID[0] == touch.fingerId)
            {
                Vector2 dif = touch.position - m_touchPos[0];
                dif.y *= -1.0f;         // プレイヤーサイドの違いを吸収
                dif.Normalize();
                m_players[0].SendMessage("SetMoveAxis",dif);
                m_players[0].SendMessage("SetFrontDirect", Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg);
                setTouchImageHandle(touch.position);
                if(dif.magnitude > 0.001f)
                {
                    m_players[0].SendMessage("SendRun");
                }
                else
                {
                    m_players[0].SendMessage("SendWait");
                }

            }

            // 2P側
            if (m_fingerID[1] < 0 && Utility.CheckRange(touch.position, m_playerMoveField[1], Screen.width * 0.5f, Screen.height * 0.5f))
            {
                m_fingerID[1] = touch.fingerId;
                m_touchPos[1] = touch.position;
            }

            if(m_fingerID[1] == touch.fingerId)
            {
                Vector2 dif = touch.position - m_touchPos[1];
                dif.x *= -1.0f;
                dif.Normalize();
                m_players[1].SendMessage("SetMoveAxis",dif);
            }


            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // アクションボタン検知
            if (Utility.CheckRange(touch.position, Vector2.zero, 200.0f, 200.0f))
            {
                m_players[0].SendMessage("SetAura", true);
            }

            if (Utility.CheckRange(touch.position, m_p2actButtonStartPos, 200.0f, 200.0f))
            {
                m_players[1].SendMessage("SetAura", true);
            }
        }

        for(int i = 0; i < m_fingerID.Length; i++)
        {
            if(m_fingerID[i] != -1 && m_saveFingerID[m_fingerID[i]] == false)
            {
                m_fingerID[i] = -1;
                m_players[i].SendMessage("SetMoveAxis", Vector2.zero);
                m_players[i].SendMessage("SendWait");
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections;

public class Utility{

	// 指定された範囲にその座標が存在するかどうか
    static public bool CheckRange(float pointX, float pointY, float startX, float startY, float width, float height)
    {
        if( startX < pointX && 
            (startX + width) > pointX &&
            startY < pointY &&
            (startY + height) > pointY)
        {
            return true;
        }
        return false;
    }

    static public bool CheckRange(Vector2 point, Vector2 start, float width, float height)
    {
        return CheckRange(point.x, point.y, start.x, start.y, width, height);
    }
}

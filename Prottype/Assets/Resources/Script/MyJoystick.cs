﻿using UnityEngine;
using System.Collections;

public class MyJoystick : MonoBehaviour {
    //======================================================================================================
    // 定数
    //======================================================================================================


    //======================================================================================================
    // 変数 
    //======================================================================================================
    //--public-----------------------------------
    public float m_renge = 50.0f;
    public GameObject m_player;

    //--private----------------------------------
    private Vector3 m_basePosition;
    private Vector3 m_firstTouchPoint;


    //=======================================================================================================
    // プロパティマネージャ
    //=======================================================================================================


    //=======================================================================================================
    // 関数
    //=======================================================================================================
    //--------------------------------------------------------------------------
    // 初期化処理
    void Start()
    {
        m_basePosition = gameObject.transform.position;
    }


    //--------------------------------------------------------------------------
    // 更新処理
    void Update()
    {

    }

    //--------------------------------------------------------------------------
    // ドラッグ時の処理
    public void Drag()
    {
       
        float dist = Vector3.Distance(m_basePosition, Input.mousePosition);
        Vector3 vec = Input.mousePosition - m_basePosition;
        vec.Normalize();
        if (dist > m_renge)
        {
            Vector3 tmp = vec;
            dist = m_renge;
            tmp *= m_renge;
            tmp += m_basePosition;
            gameObject.transform.position = tmp;
            
        }
        else
        {
            gameObject.transform.position = Input.mousePosition;
        }
        vec *= (dist / m_renge);
        m_player.SendMessage("SetMoveAxis", (Vector2)vec);
        
    }

    public void EndDrag()
    {
        gameObject.transform.position = m_basePosition;
        m_player.SendMessage("SetMoveAxis", Vector2.zero);
    }
}
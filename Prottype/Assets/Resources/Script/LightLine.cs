﻿using UnityEngine;
using System.Collections;

public class LightLine : MonoBehaviour {
    //======================================================================================================
    // 定数
    //======================================================================================================


    //======================================================================================================
    // 変数
    //======================================================================================================
    //--public-----------------------------------
    public float m_lengthMax = 5.0f;

    //--private----------------------------------
    private GameObject m_player1;
    private GameObject m_player2;
    

    //=======================================================================================================
    // プロパティマネージャ
    //=======================================================================================================


    //=======================================================================================================
    // 関数
    //=======================================================================================================
    //--------------------------------------------------------------------------
    // 初期化処理
    void Start()
    {
        m_player1 = GameObject.Find("Player1");
        m_player2 = GameObject.Find("Player2");
    }


    //--------------------------------------------------------------------------
    // 更新処理
    void Update()
    {
        Vector3 thisPos = (m_player1.transform.position + m_player2.transform.position) / 2.0f;
        gameObject.transform.position = thisPos;

        // 角度の調整
        Vector3 toPlayerVec = m_player2.transform.position - m_player1.transform.position;
        toPlayerVec.Normalize();
        Quaternion rotate = Quaternion.FromToRotation(Vector3.left, toPlayerVec);
        gameObject.transform.rotation = rotate;
        
        // 長さ（スケール）の調整
        float scaleX = Vector3.Distance(m_player1.transform.position, m_player2.transform.position);
        Vector3 scale = gameObject.transform.localScale;
        scale.x = scaleX;
        gameObject.transform.localScale = scale;

        // 表示の判定
        if( m_player1.GetComponent<Player>().IsAuraFlag == true && 
            m_player2.GetComponent<Player>().IsAuraFlag == true &&
            scaleX < m_lengthMax)
        {
            gameObject.renderer.enabled = true;
        }
        else
        {
            gameObject.renderer.enabled = false;
        }

    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPGage2 : MonoBehaviour {
    //======================================================================================================
    // 定数
    //======================================================================================================


    //======================================================================================================
    // 変数
    //======================================================================================================
    //--public-----------------------------------


    //--private----------------------------------
    private GameObject m_player;


    //=======================================================================================================
    // プロパティマネージャ
    //=======================================================================================================


    //=======================================================================================================
    // 関数
    //=======================================================================================================
    //--------------------------------------------------------------------------
    // 初期化処理
    void Start()
    {
        m_player = GameObject.Find("Player2");
        gameObject.GetComponent<Text>().text = m_player.GetComponent<Player>().Hp.ToString();
    }


    //--------------------------------------------------------------------------
    // 更新処理
    void Update()
    {
        gameObject.GetComponent<Text>().text = m_player.GetComponent<Player>().Hp.ToString();
    }
}
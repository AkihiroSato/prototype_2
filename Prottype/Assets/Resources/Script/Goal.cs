﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {
//======================================================================================================
// 定数
//======================================================================================================


//======================================================================================================
// 変数
//======================================================================================================
	//--public-----------------------------------
	
	
	//--private----------------------------------
	private int m_goalCount;
	
//=======================================================================================================
// プロパティマネージャ
//=======================================================================================================


//=======================================================================================================
// 関数
//=======================================================================================================
	//--------------------------------------------------------------------------
	// 初期化処理
	void Start () 
	{
		m_goalCount = 0;
	}
	
	
	//--------------------------------------------------------------------------
	// 更新処理
	void Update () 
	{
		
	}

	//--------------------------------------------------------------------------
	// 接触判定
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			AddGoalCount();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			SubGoalCount();
		}
	}	

	//--------------------------------------------------------------------------
	// ゴールカウントを追加する
	public void AddGoalCount()
	{
		m_goalCount++;
		Debug.Log("Add");
		if(m_goalCount >= 2)
		{
			Debug.Log("Goal");
		}
	}

	//--------------------------------------------------------------------------
	// ゴールカウントを減らす
	public void SubGoalCount()
	{
		Debug.Log("Sub");
		m_goalCount--;
	}
}